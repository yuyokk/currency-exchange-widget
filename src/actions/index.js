export function addLogItem(data) {
  return {
    type: 'ADD_LOG_ITEM',
    data
  };
}
