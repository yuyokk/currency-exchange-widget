function logs(state = [], action) {
  switch (action.type) {
    case 'ADD_LOG_ITEM':
      return [{ ...action.data }, ...state];
    default:
      return state;
  }
}

export default logs;
