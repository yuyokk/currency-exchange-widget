import React from 'react';
import styled, { keyframes } from 'styled-components';

const rotate = keyframes`
  0% {
    transform: rotate(0);
  }
  50% {
    transform: rotate(180deg);
  }
  100% {
    transform: rotate(360deg);
  }
`;

const LoadingIndicatorStyled = styled.div`
  width: 22px;
  height: 22px;

  span {
    display: inline-block;
    width: 20px;
    height: 20px;
    margin: 1px;
    background: 0 0;
    border: 2px solid #ff1fa5;
    border-bottom-color: transparent;
    border-radius: 100%;
    animation: ${rotate} 0.75s 0s linear infinite;
  }
`;

function LoadingIndicator({ className }) {
  return (
    <LoadingIndicatorStyled className={className}>
      <span />
    </LoadingIndicatorStyled>
  );
}

export default LoadingIndicator;
