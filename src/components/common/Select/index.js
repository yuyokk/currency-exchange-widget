import React from 'react';
import styled from 'styled-components';

const SelectStyled = styled.select`
  display: inline-block;
  width: 100%;
  height: calc(2.25rem + 2px);
  padding: 0.375rem 1.75rem 0.375rem 0.75rem;
  line-height: 1.5;
  color: #495057;
  text-align: left;
  vertical-align: middle;
  background-color: transparent;
  background-image: url("data:image/svg+xml;charset=utf8,%3Csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 4 5'%3E%3Cpath fill='%23343a40' d='M2 0L0 2h4zm0 5L0 3h4z'/%3E%3C/svg%3E");
  background-position: right 0.75rem center;
  background-repeat: no-repeat;
  background-size: 8px 10px;
  border: 0;
  border-bottom: 1px solid #ced4da;
  border-radius: 0;
  appearance: none;
  transition: background-color 0.15s ease-in-out, border-color 0.15s ease-in-out,
    box-shadow 0.15s ease-in-out;

  &:focus {
    outline: 0;
    border-color: #0075eb;
    /* box-shadow: 0 1px #0075eb; */
  }

  &:focus::-ms-value {
    color: #495057;
    background-color: transparent;
  }

  &:disabled {
    color: #6c757d;
    background-color: #e9ecef;
  }

  &::-ms-expand {
    opacity: 0;
  }
`;

function Select({ name, options, value, onChange }) {
  return (
    <SelectStyled name={name} value={value} onChange={onChange}>
      {options.map(option => (
        <option key={option.value} value={option.value}>
          {option.label}
        </option>
      ))}
    </SelectStyled>
  );
}

Select.defaultProps = {
  options: [],
  value: ''
};

export default Select;
