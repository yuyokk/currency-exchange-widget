import React from 'react';
import styled from 'styled-components';

const InputStyled = styled.input`
  display: block;
  width: 100%;
  height: calc(2.25rem + 2px);
  padding: 0.375rem 0.75rem;
  font-size: 1rem;
  line-height: 1.5;
  color: #495057;
  background-color: transparent;
  background-clip: padding-box;
  border: 0;
  border-bottom: 1px solid #ced4da;
  border-radius: 0;
  text-align: right;
  transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;

  &::-ms-expand {
    background-color: transparent;
    border: 0;
  }

  &:focus {
    color: #495057;
    background-color: transparent;
    border-color: #0075eb;
    /* box-shadow: 0 1px #0075eb; */
    outline: 0;
  }

  &::-webkit-input-placeholder {
    color: #6c757d;
    opacity: 1;
  }

  &::-moz-placeholder {
    color: #6c757d;
    opacity: 1;
  }

  &:-ms-input-placeholder {
    color: #6c757d;
    opacity: 1;
  }

  &::-ms-input-placeholder {
    color: #6c757d;
    opacity: 1;
  }

  &::placeholder {
    color: #6c757d;
    opacity: 1;
  }

  &:disabled {
    background-color: #e9ecef;
    opacity: 1;
  }
`;

function Input({ style, value, onChange }) {
  return <InputStyled style={style} value={value} onChange={onChange} />;
}

export default Input;
