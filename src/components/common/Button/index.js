import React from 'react';
import styled, { css } from 'styled-components';

const ButtonStyled = styled.button`
  display: inline-block;
  padding: 0.4rem 0.75rem;
  font-weight: 400;
  text-align: center;
  border: 1px solid transparent;
  font-size: 1rem;
  line-height: 1.5;
  border-radius: 2rem;
  transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out,
    border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
  white-space: nowrap;
  vertical-align: middle;
  user-select: none;

  &:hover,
  &:focus {
    text-decoration: none;
  }

  &:focus {
    outline: 0;
    box-shadow: 0 0 0 0.2rem rgba(0, 123, 255, 0.25);
  }

  &:disabled {
    opacity: 0.65;
  }

  &:not(:disabled) {
    cursor: pointer;
  }

  ${props =>
    props.primary &&
    css`
      color: #fff;
      background-color: #eb008d;
      border-color: #eb008d;

      &:hover {
        color: #fff;
        background-color: #b8006e;
        border-color: #b8006e;
      }

      &:focus {
        box-shadow: 0 0 0 0.2rem rgba(0, 123, 255, 0.5);
      }

      &:disabled {
        color: #fff;
        background-color: #ff1fa5;
        border-color: #ff1fa5;
      }

      &:not(:disabled):active {
        color: #fff;
        background-color: #b8006e;
        border-color: #b8006e;
      }

      &:not(:disabled):active:focus {
        box-shadow: 0 0 0 0.2rem rgba(0, 123, 255, 0.5);
      }
    `};
`;

function Button({ style, primary, type, disabled, children, onClick }) {
  return (
    <ButtonStyled
      style={style}
      primary={primary}
      type={type}
      disabled={disabled}
      onClick={onClick}
    >
      {children}
    </ButtonStyled>
  );
}

export default Button;
