import { connect } from 'react-redux';
import Logs from './index';

const mapStateToProps = state => ({
  items: state.logItems
});

const mapDispatchToProps = dispatch => ({});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Logs);
