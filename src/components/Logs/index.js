import React from 'react';
import styled from 'styled-components';
import { formatDate } from './utils';
import { getCcySymbol } from '../../utils';

const LogsStyled = styled.div`
  position: relative;
  padding: 1rem;
  height: 300px;
  background-color: #f3f4f5;
`;

const TitleStyled = styled.div`
  height: 2rem;
  line-height: 2rem;
  font-size: 1.5rem;
  text-align: center;
`;

const ListStyled = styled.ul`
  list-style: none;
  margin: 0;
  padding: 0;

  position: absolute;
  top: 3rem;
  right: 1rem;
  bottom: 1rem;
  left: 1rem;
  color: #a0a6ad;
  overflow-y: auto;

  li {
    padding-top: 0.25rem;
    padding-bottom: 0.25rem;
  }

  li + li {
    margin-top: 0.5rem;
  }
`;

const EmphasizedTextStyled = styled.span`
  color: #191c1e;
  font-size: 1.1rem;
  font-family: 'Courier New', Courier, monospace;
  font-weight: 700;
`;

function Logs({ style, items }) {
  if (!items || !items.length) {
    return null;
  }

  return (
    <LogsStyled style={style}>
      <TitleStyled>Transactions History</TitleStyled>

      <ListStyled>
        {items.map((item, index) => (
          <li key={item.timestamp}>
            {items.length - index}. [{formatDate(item.timestamp)}]{' '}
            <EmphasizedTextStyled style={{ marginLeft: '0.5rem' }}>
              {getCcySymbol(item.ccy1)}
              {item.ccy1Amount}
            </EmphasizedTextStyled>{' '}
            =>{' '}
            <EmphasizedTextStyled>
              {getCcySymbol(item.ccy2)}
              {item.ccy2Amount}
            </EmphasizedTextStyled>{' '}
            at rate <EmphasizedTextStyled>{item.rate}</EmphasizedTextStyled>
          </li>
        ))}
      </ListStyled>
    </LogsStyled>
  );
}

Logs.defaultProps = {
  items: []
};

export default Logs;
