export function formatDate(timestamp) {
  const dateObj = new Date(timestamp);
  const hour = dateObj.getHours();
  const min = dateObj.getMinutes();
  const sec = dateObj.getSeconds();

  return `${padZero(hour)}:${padZero(min)}:${padZero(sec)}`;
}

function padZero(num) {
  return num < 10 ? `0${num}` : num;
}
