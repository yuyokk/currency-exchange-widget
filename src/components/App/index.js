import React from 'react';
import styled from 'styled-components';
import Panel from '../Panel/container';
import Logs from '../Logs/container';
import 'normalize.css';
import './styles.css';

const AppStyled = styled.div`
  display: flex;
  flex-direction: column;
  min-height: 100%;
`;

const PanelsStyled = styled.div`
  flex: 1;
  display: flex;
  justify-content: center;
  padding-top: 2rem;
  padding-bottom: 3rem;
`;

function App() {
  return (
    <AppStyled>
      <PanelsStyled>
        <Panel ccys={['GBP', 'EUR', 'USD']} />
        {/* <Panel style={{ marginLeft: '1rem' }} ccys={['CHF', 'EUR', 'USD']} /> */}
      </PanelsStyled>

      <Logs />
    </AppStyled>
  );
}

export default App;
