import { connect } from 'react-redux';
import { addLogItem } from '../../actions';
import Panel from './index';

const mapStateToProps = state => ({});

const mapDispatchToProps = dispatch => ({
  onExchangeSubmit: data => dispatch(addLogItem(data))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Panel);
