import React, { Component } from 'react';
import styled from 'styled-components';
import Input from '../common/Input';
import Select from '../common/Select';
import Button from '../common/Button';
import LoadingIndicator from '../common/LoadingIndicator';
import { getCurrencyRates } from '../../services';
import { getCcySymbol } from '../../utils';
import {
  validateAmount,
  getAvailableCcys,
  generateCcyOptions,
  multiply,
  divide
} from './utils';

const RATE_UPDATE_INTERVAL = 10; // seconds

const PanelStyled = styled.div`
  width: 300px;
  height: 200px;
  box-shadow: 0 0 10px rgba(0, 0, 0, 0.5);
`;

const TitleStyled = styled.div`
  position: relative;
  height: 2rem;
  text-align: center;
  line-height: 2rem;
  background-color: #f3f4f5;

  .loading-rates {
    position: absolute;
    top: 5px;
    right: 5px;
  }
`;

const CcyRowStyled = styled.div`
  display: flex;
  align-items: center;

  select {
    flex-basis: 5rem;
    flex-shrink: 0;
  }

  input {
    flex: 1;
  }
`;

class Panel extends Component {
  static defaultProps = {
    ccys: []
  };

  constructor(props) {
    super(props);

    const { ccys } = this.props;

    this.gettingRatesInterval = undefined;
    this.state = {
      ccy1: ccys[0],
      ccy2: ccys[1],
      ccy1Amount: '',
      ccy2Amount: '',
      rate: undefined,
      isLoadingRate: false
    };
  }

  componentDidMount() {
    const { ccys } = this.props;

    this.updateRate(ccys[0], ccys[1]);
  }

  componentWillUnmount() {
    if (this.gettingRatesInterval) {
      clearInterval(this.gettingRatesInterval);
    }
  }

  updateRate = (ccy1, ccy2) => {
    this.getRate(ccy1, ccy2);
    this.scheduleRateUpdates(ccy1, ccy2);
  };

  getRate = (ccy1, ccy2) => {
    this.setState({ isLoadingRate: true });

    getCurrencyRates(ccy1, ccy2)
      .then(rate => {
        const { ccy1Amount } = this.state;
        const newCcy2Amount = ccy1Amount ? multiply(ccy1Amount, rate) : '';

        this.setState({
          rate,
          ccy2Amount: newCcy2Amount,
          isLoadingRate: false
        });
      })
      .catch(error => {
        console.error(error);
        this.setState({ isLoadingRate: false });
      });
  };

  scheduleRateUpdates = (ccy1, ccy2) => {
    if (this.gettingRatesInterval) {
      clearInterval(this.gettingRatesInterval);
    }

    this.gettingRatesInterval = setInterval(() => {
      this.getRate(ccy1, ccy2);
    }, RATE_UPDATE_INTERVAL * 1000);
  };

  recalculateCcy1Amount = () => {
    const { ccy2Amount, rate } = this.state;

    const ccy1Amount = divide(ccy2Amount, rate);
    this.setState({ ccy1Amount: ccy1Amount || '' });
  };

  recalculateCcy2Amount = () => {
    const { ccy1Amount, rate } = this.state;

    const ccy2Amount = multiply(ccy1Amount, rate);
    this.setState({ ccy2Amount: ccy2Amount || '' });
  };

  onCcySelect = event => {
    const ccyName = event.target.name;
    const ccyValue = event.target.value;

    this.setState({ [ccyName]: ccyValue }, () => {
      this.updateRate(this.state.ccy1, this.state.ccy2);
    });
  };

  onFormSubmit = event => {
    event.preventDefault();

    const { onExchangeSubmit } = this.props;

    if (onExchangeSubmit) {
      onExchangeSubmit({
        timestamp: Date.now(),
        ccy1: this.state.ccy1,
        ccy2: this.state.ccy2,
        ccy1Amount: this.state.ccy1Amount,
        ccy2Amount: this.state.ccy2Amount,
        rate: this.state.rate
      });
    }

    this.setState({ ccy1Amount: '', ccy2Amount: '' });
  };

  render() {
    const {
      ccy1,
      ccy2,
      ccy1Amount,
      ccy2Amount,
      rate,
      isLoadingRate
    } = this.state;
    const { ccys } = this.props;

    const availableCcy1 = getAvailableCcys(ccys, ccy2);
    const availableCcy2 = getAvailableCcys(ccys, ccy1);

    const isExchangeDisabled =
      !rate || !ccy1Amount || !ccy2Amount || isLoadingRate;

    return (
      <PanelStyled style={this.props.style}>
        <TitleStyled>
          {getCcySymbol(ccy1)}1 = {getCcySymbol(ccy2)}
          {rate && <strong>{rate}</strong>}
          {isLoadingRate && <LoadingIndicator className="loading-rates" />}
        </TitleStyled>

        <form style={{ padding: '1rem' }} onSubmit={this.onFormSubmit}>
          <CcyRowStyled style={{ marginBottom: '0.5rem' }}>
            <Select
              name="ccy1"
              options={generateCcyOptions(availableCcy1)}
              value={ccy1}
              onChange={this.onCcySelect}
            />

            <Input
              value={ccy1Amount}
              onChange={e => {
                const value = e.target.value;

                if (validateAmount(value)) {
                  this.setState({ ccy1Amount: value }, () => {
                    this.recalculateCcy2Amount();
                  });
                }
              }}
            />
          </CcyRowStyled>

          <CcyRowStyled style={{ marginBottom: '1rem' }}>
            <Select
              name="ccy2"
              options={generateCcyOptions(availableCcy2)}
              value={ccy2}
              onChange={this.onCcySelect}
            />

            <Input
              value={ccy2Amount}
              onChange={e => {
                const value = e.target.value;

                if (validateAmount(value)) {
                  this.setState({ ccy2Amount: e.target.value }, () => {
                    this.recalculateCcy1Amount();
                  });
                }
              }}
            />
          </CcyRowStyled>

          <div style={{ textAlign: 'center' }}>
            <Button
              primary
              style={{ display: 'block', width: '100%' }}
              disabled={isExchangeDisabled}
            >
              Exchange
            </Button>
          </div>
        </form>
      </PanelStyled>
    );
  }
}

export default Panel;
