export function validateAmount(value) {
  if (!value) {
    return true;
  }

  return isNumeric(value);
}

export function getAvailableCcys(ccys, selectedCcy) {
  if (!selectedCcy) {
    return ccys;
  }

  return ccys.filter(ccy => ccy !== selectedCcy);
}

export function generateCcyOptions(ccys) {
  if (!ccys) {
    return undefined;
  }

  return ccys.map(ccy => ({ value: ccy, label: ccy }));
}

export function multiply(a, b) {
  if (!a || !b) {
    return undefined;
  }

  const res = Number(a) * Number(b);

  return round(res);
}

export function divide(a, b) {
  if (!a || !b) {
    return undefined;
  }

  const res = Number(a) / Number(b);

  return round(res);
}

function round(num) {
  return Math.round(num * 100) / 100;
}

function isNumeric(value) {
  const res = !isNaN(value - parseFloat(value));

  return res;
}
