export function getCcySymbol(ccy) {
  switch (ccy) {
    case 'EUR':
      return String.fromCharCode(8364);
    case 'USD':
      return String.fromCharCode(36);
    case 'GBP':
      return String.fromCharCode(163);
    case 'RUB':
      return String.fromCharCode(8381);
    case 'UAH':
      return String.fromCharCode(8372);
    default:
      return ccy;
  }
}
