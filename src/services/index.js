import axios from 'axios';

const RATES_ENDPOINT = 'https://api.exchangeratesapi.io/latest';

export function getCurrencyRates(ccy1, ccy2) {
  return axios
    .get(RATES_ENDPOINT, {
      params: {
        base: ccy1,
        symbols: ccy2
      }
    })
    .then(response => {
      const data = (response && response.data) || {};

      return data.rates[ccy2];
    });
}
